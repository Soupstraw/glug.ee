# Tartu GNU/Linux User Group

## Tere tulemast GLUG Tartu kodulehele!

Oleme uus 2020. aasta sügisel alustav organisatsioon Tartus.
Ühendame GNU/Linuxi kasutajaid, korraldame liikmetele üritusi ning levitame vaba tarkvara, federeeritud teenuste ja privaatsuse ideid.
Meie liikmed on üldiselt Tartu Ülikooli informaatikatudengid, aga oodatud on ka GNU/Linuxi kasutajad teistelt erialadelt või ülikooliväliselt.

## Tegevused

- Saame kokku ja arutame GNU/Linuxiga ja vabavaraga seotud teemadel.
- Teeme LAN-i pidusid vabade GNU/Linuxi mängudega: [OpenArena](http://www.openarena.ws/smfnews.php), [Minetest](https://www.minetest.net/), [TeeWorlds](https://www.teeworlds.com/), [SuperTuxKart](https://supertuxkart.net/Main_Page) ja muud.
- Katsetame ja kasutame aktiivselt erinevaid vabavaralisi lahendusi.
- Teeme töötubasid GNU/Linuxi, vabavara, federeeritud teenuste ja privaatsuse teemadel.
- Arendame ja toetame vabavaralisi projekte.

## Kontakt ja liitumine!

Kui tahad meiega ühendust võtta või meiega liituda, saada meil aadressile [glugtartu@pm.me](mailto:glugtartu@pm.me). Liitumisavalduse puhul kirjuta, miks Sa tahad liituda.

## Meid toetavad

Praegu toetavad meid ainult GLUG Tartu liikmed.

Siin võiks olla Sinu ettevõte! Kirjuta meile koostöö kohta. Otsime rahastust ürituste korraldamiseks ning serverite ülalhoidmiseks.
