# Liitu meiega!

Ootame uusi liikmeid, kes tahavad meie tegemistega kaasa aidata. Kirjuta aadressile [glugtartu@pm.me](mailto:glugtartu@pm.me), miks Sa tahad liituda.
