FROM python:3.8-alpine AS builder

COPY . .
RUN pip3 install -r requirements.txt
RUN mkdir -p /var/www/glug.ee/html
RUN python3 htmlgen.py

FROM nginx:alpine
COPY --from=builder /var/www/glug.ee /var/www/glug.ee
